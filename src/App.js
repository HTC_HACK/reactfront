import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Task from "./component/Task";

function App() {
  return (
    <div className="App">
        <div class={"container"}><Task/></div>
    </div>
  );
}

export default App;
