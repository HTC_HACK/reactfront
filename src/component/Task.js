import React, {Component} from 'react';
import axios from "axios";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Button from '@mui/material/Button';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import {Input} from "reactstrap";


class Task extends Component {

    componentDidMount() {
        axios.get("http://localhost:8080/api/tasks")
            .then(res => {
                this.setState({list: res.data})
            })
    }

    state = {
        "list": [],
        "title": "",
        "description": ""
    }

    render() {
        const {title, description, list} = this.state;

        const handleSubmit = (e) => {
            e.preventDefault();
            axios.post("http://localhost:8080/api/tasks", {title, description})
                .then(res => {
                    alert(res.data)
                    this.componentDidMount();
                })
        }

        const handleDelete = (id) => {
            axios.delete("http://localhost:8080/api/tasks/" + id)
                .then(res => {
                    alert(res.data)
                    this.componentDidMount();
                })
        }

        const handleChange = (e) => {
            const name = e.target.name;
            const value = e.target.value;
            this.setState(values => ({...values, [name]: value}))
        }
        return (


            <div class={"container"} style={{padding: '5%'}}>
                <form onSubmit={handleSubmit} style={{padding: '5%'}}>
                    <div>
                        <label>
                            <Input onChange={handleChange} placeholder={"Title"} type="text" name="title" required/>
                        </label>

                    </div>
                    <br/>
                    <div>
                        <label>
                            <Input onChange={handleChange} placeholder={"Description"} type="text" name="description" required/>
                        </label>
                    </div>
                    <br/>
                    <div>
                        <Button variant="contained" type={"submit"}>
                            Save
                        </Button>
                    </div>
                </form>
                <TableContainer component={Paper}>
                    <Table sx={{minWidth: 650}} aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell>#</TableCell>
                                <TableCell align="left">Title</TableCell>
                                <TableCell align="left">Description</TableCell>
                                <TableCell align="left">Edit</TableCell>
                                <TableCell align="left">Delete</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {list.map((row) => (
                                <TableRow
                                    key={row.id}
                                    sx={{'&:last-child td, &:last-child th': {border: 0}}}
                                >
                                    <TableCell component="th" scope="row">
                                        {row.id}
                                    </TableCell>
                                    <TableCell align="left">{row.title}</TableCell>
                                    <TableCell align="left">{row.description}</TableCell>
                                    <TableCell align="left">Edit</TableCell>
                                    <TableCell align="left">
                                        <Button variant="outlined" onClick={() => handleDelete(row.id)}
                                                startIcon={<DeleteIcon/>}>
                                            Delete
                                        </Button>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </div>
        );
    }
}


export default Task;



